package org.springrest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.springrest.beans.BolsaFamilia;
import org.springrest.beans.BolsaFamiliaAgregation;
import org.springrest.dao.BolsaFamiliaDAO;

@RestController
public class BolsaFamiliaController {
	
	@Autowired
	private BolsaFamiliaDAO bolsaFamiliaDao;
	
	@RequestMapping(value = "/bolsafamilia/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	@Cacheable(value="bolsafamilia")
	public ResponseEntity<BolsaFamilia> getBolsaFamilia(@PathVariable("id") String id) {
		BolsaFamilia bolsaFamilia;
		try {
			bolsaFamilia = bolsaFamiliaDao.findById(id);
			if (bolsaFamilia == null) {
				System.out.println("Bolsa Familia id " + id + " not found");
				return new ResponseEntity<BolsaFamilia>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<BolsaFamilia>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<BolsaFamilia>(bolsaFamilia, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/bolsafamilia/agregado/{uf}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	@Cacheable(value="bolsafamilia")
	public ResponseEntity<List<BolsaFamiliaAgregation>> getBolsaFamiliaAgregadaPorMunicipio(@PathVariable("uf") String uf) {
		List<BolsaFamiliaAgregation> bolsaFamiliaAgregation;
		try {
			bolsaFamiliaAgregation = bolsaFamiliaDao.getAgregationByEstado(uf);
			if (bolsaFamiliaAgregation == null) {
				return new ResponseEntity<List<BolsaFamiliaAgregation>>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<BolsaFamiliaAgregation>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<List<BolsaFamiliaAgregation>>(bolsaFamiliaAgregation, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/bolsafamilia/nomes/{nome}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	@Cacheable(value="bolsafamilia")
	public ResponseEntity<List<BolsaFamilia>> listAllBolsaFamilia(@PathVariable("nome") String nome) {
		List<BolsaFamilia> bolsasFamilia;
		try {
			bolsasFamilia = bolsaFamiliaDao.findAll(nome);
			if (bolsasFamilia == null) {
				System.out.println("Bolsa Familia nome " + nome + " not found");
				return new ResponseEntity<List<BolsaFamilia>>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<BolsaFamilia>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<List<BolsaFamilia>>(bolsasFamilia, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/bolsafamilia", method = RequestMethod.POST)
	public ResponseEntity<Void> createBolsaFamilia(@RequestBody BolsaFamilia bolsaFamilia, UriComponentsBuilder ucBuilder) {
		System.out.println("Creating bolsa familia " + bolsaFamilia);
		bolsaFamiliaDao.insert(bolsaFamilia);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/bolsafamilia/{id}").buildAndExpand(bolsaFamilia.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

}
