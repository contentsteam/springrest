package org.springrest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.springrest.beans.Livro;
import org.springrest.services.LojaService;

@RestController
public class LojaController {

	@Autowired
	private LojaService lojaService;

	@RequestMapping(value = "/livrobasico", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, headers = { "Accept=application/xml" })
	public ResponseEntity<Livro> getLivro() {
		return new ResponseEntity<Livro>(new Livro(1l, "Poeira em alto mar", 20.0), HttpStatus.OK);
	}

	// -------------------Retrieve All
	// Livros--------------------------------------------------------

	@RequestMapping(value = "/livro/", method = RequestMethod.GET)
	public ResponseEntity<List<Livro>> listAllLivros() {
		List<Livro> Livros = lojaService.findAllLivros();
		if (Livros.isEmpty()) {
			return new ResponseEntity<List<Livro>>(HttpStatus.NO_CONTENT);// o retorno indica que não existe nada
		}
		return new ResponseEntity<List<Livro>>(Livros, HttpStatus.OK);
	}

	// -------------------Retrieve Single
	// Livro--------------------------------------------------------

	@RequestMapping(value = "/livro/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Livro> getLivro(@PathVariable("id") long id) {
		System.out.println("Lendo Livro de id " + id);
		Livro Livro = lojaService.findById(id);
		if (Livro == null) {
			System.out.println("Livro de id " + id + " não encontrado");
			return new ResponseEntity<Livro>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Livro>(Livro, HttpStatus.OK);
	}

	// -------------------Create a
	// Livro--------------------------------------------------------

	@RequestMapping(value = "/livro/", method = RequestMethod.POST)
	public ResponseEntity<Void> createLivro(@RequestBody Livro Livro, UriComponentsBuilder ucBuilder) {
		System.out.println("Criando Livro " + Livro.getNome());

		if (lojaService.isLivroExist(Livro)) {
			System.out.println("A Livro de nome " + Livro.getNome() + " já existe");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}

		lojaService.saveLivro(Livro);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/Livro/{id}").buildAndExpand(Livro.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	// ------------------- Update a Livro
	// --------------------------------------------------------

	@RequestMapping(value = "/livro/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Livro> updateLivro(@PathVariable("id") long id, @RequestBody Livro Livro) {
		System.out.println("Atualizando Livro " + id);

		Livro currentLivro = lojaService.findById(id);

		if (currentLivro == null) {
			System.out.println("Livro de id " + id + " não encontrado");
			return new ResponseEntity<Livro>(HttpStatus.NOT_FOUND);
		}

		currentLivro.setNome(Livro.getNome());
		currentLivro.setPreco(Livro.getPreco());

		lojaService.updateLivro(currentLivro);
		return new ResponseEntity<Livro>(currentLivro, HttpStatus.OK);
	}

	// ------------------- Delete a Livro
	// --------------------------------------------------------

	@RequestMapping(value = "/livro/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Livro> deleteLivro(@PathVariable("id") long id) {
		System.out.println("Lendo & Apagando o Livro de id " + id);

		Livro Livro = lojaService.findById(id);
		if (Livro == null) {
			System.out.println("Não foi possível apagar o livro. Livro de id " + id + " não foi encontrado");
			return new ResponseEntity<Livro>(HttpStatus.NOT_FOUND);
		}

		lojaService.deleteLivroById(id);
		return new ResponseEntity<Livro>(HttpStatus.NO_CONTENT);
	}

	// ------------------- Delete All Livros
	// --------------------------------------------------------

	@RequestMapping(value = "/livro/", method = RequestMethod.DELETE)
	public ResponseEntity<Livro> deleteAllLivros() {
		System.out.println("Apagando todos os Livros");

		lojaService.deleteAllLivros();
		return new ResponseEntity<Livro>(HttpStatus.NO_CONTENT);
	}

}
