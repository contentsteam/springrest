package org.springrest.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.stereotype.Service;

import org.springrest.beans.Livro;

@Service
public class LojaService{
	
	private static final AtomicLong counter = new AtomicLong();
	
	private static List<Livro> listaLivros;
    
    static{
        listaLivros= populateLivros();
    }
 
	public List<Livro> findAllLivros() {
        return listaLivros;
    }
     
	public Livro findById(long id) {
        for(Livro Livro : listaLivros){
            if(Livro.getId() == id){
                return Livro;
            }
        }
        return null;
    }
     
	public Livro findByName(String name) {
        for(Livro Livro : listaLivros){
            if(Livro.getNome().equalsIgnoreCase(name)){
                return Livro;
            }
        }
        return null;
    }
     
	public void saveLivro(Livro Livro) {
        Livro.setId(counter.incrementAndGet());
        listaLivros.add(Livro);
    }
 
	public void updateLivro(Livro Livro) {
        int index = listaLivros.indexOf(Livro);
        listaLivros.set(index, Livro);
    }
 
	public void deleteLivroById(long id) {
        for (Iterator<Livro> iterator = listaLivros.iterator(); iterator.hasNext(); ) {
            Livro Livro = iterator.next();
            if (Livro.getId() == id) {
                iterator.remove();
            }
        }
    }
 
	public boolean isLivroExist(Livro Livro) {
        return findByName(Livro.getNome())!=null;
    }
     
	public void deleteAllLivros(){
        listaLivros.clear();
    }
	
	private static List<Livro> populateLivros(){
        List<Livro> Livros = new ArrayList<Livro>();
        Livros.add(new Livro(1l, "Poeira em alto mar", 20.0));
        Livros.add(new Livro(2l, "A volta dos que não foram", 30.0));
        Livros.add(new Livro(3l, "Voltei não indo", 30.0));
        Livros.add(new Livro(4l, "A volta dos que não foram", 40.0));
        return Livros;
    }

}