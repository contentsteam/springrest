package org.springrest.beans;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="pagamentos")
public class BolsaFamilia {
	
	@Id
	private String id;
	
	private String uf;
	
	private String municipio;
	
	private String nome;
	
	private String programa;
	
	private Double valor;
	
	private Date data;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPrograma() {
		return programa;
	}

	public void setPrograma(String programa) {
		this.programa = programa;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "BolsaFamilia [id=" + id + ", uf=" + uf + ", municipio=" + municipio + ", nome=" + nome + ", programa="
				+ programa + ", valor=" + valor + ", data=" + data + "]";
	}
	
	

}
