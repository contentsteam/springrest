package org.springrest.beans;

import org.springframework.data.mongodb.core.mapping.Field;

public class BolsaFamiliaAgregation {
	
	@Field("_id")
	private String municipio;

	private Long total;
	
	private Double valorTotal;

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	
	
}
