package org.springrest.dao;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.previousOperation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springrest.beans.BolsaFamilia;
import org.springrest.beans.BolsaFamiliaAgregation;

@Service
public class BolsaFamiliaDAO {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public BolsaFamilia findById(String id) throws Exception{
		System.out.println("Search id - " + id);
		Query queryDeBuscaPorID = new Query(Criteria.where("id").is(id));
		return mongoTemplate.findOne(queryDeBuscaPorID,BolsaFamilia.class);
	}
	
	public List<BolsaFamilia> findAll(String nome) {
		List<BolsaFamilia> bolsasFamilias = new ArrayList<BolsaFamilia>();
		
		Criteria regex = Criteria.where("nome").regex(nome, "i");
		bolsasFamilias = mongoTemplate.find(new Query().addCriteria(regex), BolsaFamilia.class);
		System.out.println("total de bolsas familia = " + bolsasFamilias.size());
		return bolsasFamilias;
	}
	
	public void insert(BolsaFamilia bolsaFamilia) {
		mongoTemplate.insert(bolsaFamilia);
		System.out.println("novo bolsaFamiliao = " + bolsaFamilia);
	}
	
	public List<BolsaFamiliaAgregation> getAgregationByEstado(String uf) {
		Aggregation aggregation = newAggregation(
				match(Criteria.where("uf").is(uf)),
				group("municipio").count().as("total")
		        .sum("valor").as("valorTotal"),
		        sort(Sort.Direction.DESC, "valorTotal") );
		AggregationResults<BolsaFamiliaAgregation> groupResults = mongoTemplate.aggregate(aggregation, BolsaFamilia.class, BolsaFamiliaAgregation.class);
		List<BolsaFamiliaAgregation> result = groupResults.getMappedResults();
		
		return result;
	}

}